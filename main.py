import argparse
import atexit
import decimal
import logging
import sys
from pathlib import Path

import diskcache
from PyQt5.QtCore import QSettings
from PyQt5.QtWidgets import QApplication

from crypto_overview import constants
from crypto_overview.models.asset import AssetRepository
from crypto_overview.models.price import PriceRepository
from crypto_overview.models.currency import Currency
from crypto_overview.models.wallets import WalletRepository
from crypto_overview.settings import init_settings
from crypto_overview.ui.main_window import MainWindow
from crypto_overview.utils.log import init_log

# Decimal config
decimal.DefaultContext.prec = 45
decimal.setcontext(decimal.DefaultContext)

ORGANIZATION_NAME = constants.NAME
APPLICATION_NAME = constants.NAME


def run(args):
    # Create application
    QApplication.setOrganizationName(ORGANIZATION_NAME)
    QApplication.setApplicationName(APPLICATION_NAME)
    app = QApplication(args)

    # Set application style
    app.setStyle("Fusion")
    qss_file = "resources/qss/main.qss"
    with open(qss_file, "r") as fh:
        app.setStyleSheet(fh.read())

    # Init settings
    init_settings()
    settings = QSettings()

    # Create cache
    cache_directory = Path.home() / f".{APPLICATION_NAME.lower()}" / "cache"
    cache = diskcache.Cache(cache_directory.as_posix(), size_limit=5e7)

    # Create models
    asset_repository = AssetRepository(cache)
    price_repository = PriceRepository(cache, currency=Currency(settings.value("state/currency")))
    wallet_repo = WalletRepository(asset_repository, price_repository)

    # Create main window
    main_window = MainWindow(wallet_repo, asset_repository, price_repository)
    main_window.show()

    # Register function on exit event
    @atexit.register
    def exit_handler():
        cache.close()

    app.exec()
    # sys.exit(app.exec())


def create_parser() -> argparse.ArgumentParser:
    """
    Create parser for the simple CLI used when launching the application.
    """

    argument_parser = argparse.ArgumentParser(description=f"{APPLICATION_NAME} application")

    argument_parser.add_argument("--log-file", type=Path, required=False, default=None, help="Log file")
    argument_parser.add_argument("--log-level", type=str, required=False, default="warning",
                                 choices=["debug", "info", "warning", "error", "fatal"])

    return argument_parser


if __name__ == "__main__":
    args = create_parser().parse_args(sys.argv[1:])

    # Init logging
    log_file = args.log_file
    if log_file is None:
        log_file = Path.home() / f".{APPLICATION_NAME.lower()}" / "app.log"
    init_log(file_path=log_file, log_level=args.log_level)

    logger = logging.getLogger()

    try:
        run(sys.argv)
    except RuntimeError as e:
        logger.exception("Unexpected exception! %s", e)
