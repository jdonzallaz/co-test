FROM python:3.9

WORKDIR /app

COPY ./requirements.txt /app/requirements.txt

RUN python --version
RUN pip --version

RUN pip install --no-cache-dir --upgrade pip

RUN pip install --no-cache-dir --trusted-host pypi.python.org -r requirements.txt

RUN apt-get update; \
#    apt-get install -y --no-install-recommends ffmpeg libsm6 libxext6
    apt-get install -y --no-install-recommends libgl1
#    apt-get install -y --no-install-recommends libgl1-mesa-glx
#
#RUN sudo apt install libgl1-mesa-glx
#RUN apt-get install ffmpeg libsm6 libxext6  -y
#python3-opencv

# Copy the current directory contents into the container at /app
COPY . /app/

# Configure exposed port
#ENV QT_QPA_PLATFORM="offscreen"

# Run the app
#RUN ls
#RUN pytest tests -v --junitxml=report-tests.xml
#RUN pytest crypto_overview --doctest-modules -v --junitxml=report-doctests.xml
#RUN ls -alh

RUN pyinstaller crypto-overview.spec
RUN ls -alh
RUN ls dist/crypto-overview/ -alh
#RUN ./dist/main/main
