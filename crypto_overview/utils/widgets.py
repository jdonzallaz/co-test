from PyQt5 import QtWidgets


def clear_layout(layout: QtWidgets.QLayout):
    """
    Remove all children from a layout.

    :param layout: The layout from which to remove the children
    """

    if layout is not None:
        while layout.count():
            item = layout.takeAt(0)
            widget = item.widget()
            if widget is not None:
                widget.deleteLater()  # equivalent to widget.setParent(None)
            else:
                clear_layout(item.layout())


def remove_layout(layout: QtWidgets.QLayout):
    """
    Remove a layout from the UI and its parent. Also remove all its children.

    :param layout: The layout to remove
    """

    clear_layout(layout)
    layout.parent().removeItem(layout)
