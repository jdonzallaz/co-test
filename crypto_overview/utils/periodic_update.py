import abc

from PyQt5 import QtCore


class PeriodicUpdate:
    """
    Make a class periodically update itself. It launches a timer, which will periodically call
    the abstract "periodic_update" method.
    The update interval is defined in a settings (accessible with QSettings()) in seconds,
    and the key is given to the constructor.
    When the update interval is updated in the settings, the "update_periodic_update_interval" method must be called,
    which will update the interval and start/stop/keep the timer depending on the value.
    An update interval of "0" disables the periodic update.
    """

    __metaclass__ = abc.ABCMeta

    def __init__(self, interval_settings: str):
        self.interval_settings = interval_settings
        self.settings = QtCore.QSettings()

        self.periodic_update_timer = QtCore.QTimer()
        self.periodic_update_timer.timeout.connect(self.periodic_update)
        self.update_periodic_update_interval()

    @abc.abstractmethod
    def periodic_update(self):
        pass

    def update_periodic_update_interval(self):
        interval = self.settings.value(self.interval_settings) * 1000

        if interval <= 0:
            self.periodic_update_timer.stop()

        self.periodic_update_timer.setInterval(interval)

        if interval > 0:
            self.periodic_update_timer.start()
