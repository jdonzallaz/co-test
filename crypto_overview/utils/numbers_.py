from decimal import Decimal
from typing import Callable, Union


def float_to_decimal(x: float) -> Decimal:
    """
    Create a Decimal object from a float, by trying to avoid loss of precision.

    :param x: a float
    :return: the corresponding Decimal object
    """

    return Decimal(f"{x:.14f}").normalize()


def float_to_str(f, n_significant: int = 5) -> str:
    """
    Format a float to string with a given number of significant figures.
    If the number is over or equal to 10^({n_significant}-1), all of the figures before the decimal character are kept.
    Otherwise, it shows the float with 5 significant figures, in positional mode.

    >>> float_to_str(1234567.89)
    '1234568'
    >>> float_to_str(123.45678)
    '123.46'
    >>> float_to_str(1000.00001)
    '1000'
    >>> float_to_str(0.00012345678)
    '0.00012346'
    >>> float_to_str(1.0230000045)
    '1.023'

    :param f: the float to format
    :param n_significant: the minimum number of significant digit to keep
    :return: the formatted float as string
    """

    if abs(f) >= 10 ** (n_significant - 1):
        return str(round(f))

    float_string = "{:.{}g}".format(f, n_significant)
    if "e" in float_string:  # detect scientific notation
        digits, exp = float_string.split("e")
        digits = digits.replace(".", "").replace("-", "")
        exp = int(exp)
        zero_padding = "0" * (abs(exp) - 1)  # minus 1 for decimal point in the sci notation
        sign = "-" if f < 0 else ""
        if exp > 0:
            float_string = "{}{}{}.0".format(sign, digits, zero_padding)
        else:
            float_string = "{}0.{}{}".format(sign, zero_padding, digits)

    return float_string


def create_convert_decimal_unit(exponent: int) -> Callable:
    """
    Convert an amount from raw unit to different unit by given exponent.
    This function create a new function, creating the actual conversion function.

    E.g. 1 NANO = 1 Mnano = 1000000000000000000000000000000 raw nano (10^30)

    >>> create_convert_decimal_unit(30)(1000000000000000000000000000000)
    Decimal('1')
    >>> create_convert_decimal_unit(30)(100000000000000000000000000000)
    Decimal('0.1')
    >>> create_convert_decimal_unit(30)(38001200000000000000000000000000)
    Decimal('38.0012')

    :param exponent: the exponent, by which to divide the raw amount
    :return: a function to convert an amount with the given exponent
    """

    denom = Decimal(10**exponent)

    def convert_decimal_unit(raw_amount: Union[int, Decimal]) -> Decimal:
        """
        Convert an amount from raw unit to different unit by given exponent.

        :param raw_amount: the raw amount to convert
        :return: the converted amount
        """

        return Decimal(raw_amount) / denom

    return convert_decimal_unit
