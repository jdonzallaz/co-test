from typing import Callable


class Observable:
    """
    Make a class observable. Other code can subscribe to the observable by adding an observer,
    that will be called when the observable "emit".
    """

    @property
    def observers(self) -> set[Callable]:
        if not hasattr(self, "_observers"):
            self._observers: set[Callable] = set()
        return self._observers

    def add_observer(self, fn: Callable):
        self.observers.add(fn)

    def remove_observer(self, fn: Callable):
        self.observers.remove(fn)

    def emit(self, *args, **kwargs):
        for observer in self.observers:
            observer(*args, **kwargs)
