from enum import Enum

from crypto_overview.wallet.algorand_wallet import AlgorandWallet
from crypto_overview.wallet.binance_wallet import BinanceWallet
from crypto_overview.wallet.custom_wallet import CustomWallet
from crypto_overview.wallet.exchange_wallet import ExchangeWallet
from crypto_overview.wallet.nano_wallet import NanoWallet
from crypto_overview.wallet.wallet import Wallet


class WalletType(Enum):
    Algorand = "algorand", "Algorand", AlgorandWallet
    Binance = "binance", "Binance", BinanceWallet
    Exchange = "exchange", "Exchange", ExchangeWallet
    Custom = "custom", "Custom", CustomWallet
    Nano = "nano", "Nano", NanoWallet

    def __new__(cls, id: str, friendly_name: str, wallet_class: type[Wallet]):
        obj = object.__new__(cls)
        obj._value_ = id
        obj.id = id
        obj.friendly_name = friendly_name
        obj.cls = wallet_class
        return obj

    @staticmethod
    def get_id_from_class(obj) -> str:
        for w in WalletType:
            if isinstance(obj, w.cls):
                return w.id
        raise RuntimeError("Wallet class unknown")
