# How to add a new wallet

- Subclass `crypto_overview.wallet.wallet.Wallet`.
    - Implement `get_balances()`.
    - Add an optional `__init__()`.
        - The first parameters must be `self, asset_repository: AssetRepository, name: WalletNameType`, and the super()
          initializer must be called.
        - The others optional arguments must use the `Annotated[]` type syntax (the regex and custom form are optional):
          - `Annotated[{type}, "{friendly name}", r"{validation regex}", "{name of custom form}"]`
          - e.g. `Annotated[str, "Name", r"([A-Za-z])([\w\d\s.-_\[\]])+"]`
        - The optional arguments must be *saved* in the class with the same name as the argument (e.g.
          `self.my_arg = my_arg`).
    - An optional `description` property can be added to the class.
- Add the new type to `crypto_overview.wallet.wallet_type.WalletType`. Add a new entry to the enum with id, friendly
  name and class reference.
- If one of your argument is more complex than just a simple string or number, you can create a custom form.
  Subclass `CustomWalletForm`, taking existing forms as examples. The name of the class must be added to
  the `Annotated[]` type of the argument, and the creation added to the `form_factory.create_wallet_form` factory
  function.
