import abc
import inspect
import typing
from typing import Any, Optional, _AnnotatedAlias

from crypto_overview.models.asset import AssetRepository
from crypto_overview.models.balance import Balance

WalletNameType = typing.Annotated[str, "Name", r"([A-Za-z])([\w\d\s.-_\[\]])+"]


class Wallet:
    """
    Abstract base class for all types of wallets.
    get_balances must be overridden and return the balances for the wallet.
    Forms for wallets creation are created using the constructor arguments, which must use the Annotated types format.
    An optional description can be added as class attribute.
    """

    __metaclass__ = abc.ABCMeta

    description: Optional[str] = None

    def __init__(self, asset_repository: AssetRepository, name: WalletNameType):
        self.asset_repository = asset_repository
        self.name = name

    @abc.abstractmethod
    def get_balances(self) -> list[Balance]:
        pass

    @classmethod
    def get_required_parameters(cls) -> list[str]:
        return [arg.name for arg in inspect.signature(cls).parameters.values()][1:]

    @classmethod
    def get_required_parameters_friendly_name(cls):
        return {
            arg.name: arg.annotation.__metadata__[0] if isinstance(arg.annotation, _AnnotatedAlias) else arg.name
            for arg in list(inspect.signature(cls).parameters.values())[1:]
        }

    @classmethod
    def get_required_parameters_types(cls) -> dict[str, Any]:
        all_types = typing.get_type_hints(cls.__init__)
        return {key: all_types[key] for key in cls.get_required_parameters()}

    @classmethod
    def get_required_parameters_regex(cls):
        return {
            arg.name: arg.annotation.__metadata__[1]
            if isinstance(arg.annotation, _AnnotatedAlias) and len(arg.annotation.__metadata__) > 1
            else None
            for arg in list(inspect.signature(cls).parameters.values())[1:]
        }

    @classmethod
    def get_required_parameters_form_class(cls):
        return {
            arg.name: arg.annotation.__metadata__[2]
            if isinstance(arg.annotation, _AnnotatedAlias) and len(arg.annotation.__metadata__) > 2
            else None
            for arg in list(inspect.signature(cls).parameters.values())[1:]
        }

    def get_name(self) -> str:
        return self.name
