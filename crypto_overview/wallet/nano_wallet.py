from decimal import Decimal
from typing import Annotated

import requests

from crypto_overview.models.asset import AssetRepository
from crypto_overview.models.balance import Balance
from crypto_overview.utils.numbers_ import create_convert_decimal_unit
from crypto_overview.wallet.wallet import Wallet, WalletNameType

DEFAULT_NANO_API = "https://nault.nanos.cc/proxy"

NanoPublicAddressType = Annotated[str, "Public Address", r"nano_([a-z0-9]){60}"]


class NanoWallet(Wallet):
    api: str = DEFAULT_NANO_API
    description: str = (
        f"Connect to the your wallet on the Nano blockchain. "
        f'Get your XNO balance using the "{DEFAULT_NANO_API}" API.'
    )

    def __init__(
        self,
        asset_repository: AssetRepository,
        name: WalletNameType,
        public_address: NanoPublicAddressType,
    ):
        super().__init__(asset_repository, name)
        self.public_address = public_address
        self.nano_convert_amount_raw_to_mnano = create_convert_decimal_unit(30)

    def get_balances(self) -> list[Balance]:
        post_data = {"action": "account_balance", "account": self.public_address}
        r = requests.post(self.api, json=post_data)
        data = r.json()
        balance = self.nano_convert_amount_raw_to_mnano(int(data["balance"]))
        asset = self.asset_repository.get_by_symbol_or_id("nano")

        return [Balance(asset, free=balance, locked=Decimal(0), rewards=Decimal(0))]
