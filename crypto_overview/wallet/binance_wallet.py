from decimal import Decimal
from typing import Annotated

from binance import Client

from crypto_overview.models.asset import AssetRepository
from crypto_overview.models.balance import Balance
from crypto_overview.wallet.wallet import Wallet, WalletNameType

BinanceApiKeyType = Annotated[str, "API key", r"([A-Za-z0-9]){64}"]
BinanceApiSecretType = Annotated[str, "API secret", r"([A-Za-z0-9]){64}"]


class BinanceWallet(Wallet):
    description: str = f"Connect to your Binance spot wallet."

    def __init__(
        self,
        asset_repository: AssetRepository,
        name: WalletNameType,
        api_key: BinanceApiKeyType,
        api_secret: BinanceApiSecretType,
    ):
        super().__init__(asset_repository, name)
        self.api_key = api_key
        self.api_secret = api_secret

        self.client = Client(self.api_key, self.api_secret)

    def get_balances(self) -> list[Balance]:
        balances_data = self.client.get_account()["balances"]
        balances_data = map(
            lambda b: ({**b, "free": Decimal(b["free"]), "locked": Decimal(b["locked"])}), balances_data
        )
        balances_data = (b for b in balances_data if b["free"] > 0 or b["locked"] > 0)

        balances = []
        for balance in balances_data:
            asset = self.asset_repository.get_by_symbol_or_id(balance["asset"])
            balance = Balance(asset, free=balance["free"], locked=balance["locked"], rewards=Decimal(0))
            balances.append(balance)

        return balances
