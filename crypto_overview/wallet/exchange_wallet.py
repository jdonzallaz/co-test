import logging
from dataclasses import dataclass
from decimal import Decimal
from typing import Annotated, Protocol

import ccxt

from crypto_overview.models.asset import Asset, AssetRepository
from crypto_overview.models.balance import Balance
from crypto_overview.wallet.wallet import Wallet, WalletNameType

logger = logging.getLogger()


class BalanceTransform(Protocol):
    def __call__(self, balances: list[Balance], asset_repository: AssetRepository) -> list[Balance]:
        pass


def fix_binance_staking(balances: list[Balance], asset_repository: AssetRepository) -> list[Balance]:
    asset_balance_dict: dict[Asset, Balance] = {}
    for balance in balances:
        if balance.asset.symbol.startswith("LD"):
            symbol = balance.asset.symbol.replace("LD", "")
            asset = asset_repository.get_by_symbol(symbol)
            if asset.id != "unknown":
                balance = Balance(asset, Decimal(0), balance.free + balance.locked, balance.rewards)

        if balance.asset in asset_balance_dict:
            asset_balance_dict[balance.asset] += balance
        else:
            asset_balance_dict[balance.asset] = balance

    return list(asset_balance_dict.values())


@dataclass
class ExchangeCredential:
    apiKey: str = ""
    secret: str = ""
    uid: str = ""
    login: str = ""
    password: str = ""
    twofa: str = ""
    privateKey: str = ""
    walletAddress: str = ""
    token: str = ""


@dataclass
class ExchangeConfig:
    id: str
    credentials: ExchangeCredential


ExchangeConfigType = Annotated[ExchangeConfig, "Exchange", "", "ExchangeForm"]


class ExchangeWallet(Wallet):
    description: str = f"Use CCXT to connect to any exchange."

    def __init__(self, asset_repository: AssetRepository, name: WalletNameType, config: ExchangeConfigType):
        super().__init__(asset_repository, name)
        self.config = config
        self.exchange_id = config.id
        self.exchange_class = getattr(ccxt, self.exchange_id)
        self.exchange: ccxt.Exchange = self.exchange_class(config.credentials.__dict__)

        self.transforms: dict[BalanceTransform, set[str]] = {fix_binance_staking: {"binance"}}

    def get_balances(self) -> list[Balance]:
        try:
            balances_data = self.exchange.fetch_balance()
        except ccxt.NetworkError as e:
            msg = f"fetch_balance failed due to a network error. Wallet={self.name}, Exchange={self.exchange_id}"
            logger.error(msg, exc_info=e)
            raise type(e)(msg) from e
        except ccxt.ExchangeError as e:
            msg = f"fetch_balance failed due to an exchange error. Wallet={self.name}, Exchange={self.exchange_id}"
            logger.error(msg, exc_info=e)
            raise type(e)(msg) from e
        except Exception as e:
            msg = f"fetch_balance failed due to an unknown error. Wallet={self.name}, Exchange={self.exchange_id}"
            logger.error(msg, exc_info=e)
            raise type(e)(msg) from e

        balances = []
        for asset_symbol, total_balance in balances_data["total"].items():
            if total_balance > 0 and not self.should_ignore_asset(asset_symbol):
                asset = self.asset_repository.get_by_symbol_or_id(asset_symbol)
                asset_balance = balances_data[asset_symbol]
                balance = Balance(
                    asset,
                    free=Decimal(asset_balance["free"]),
                    locked=Decimal(asset_balance["used"]),
                    rewards=Decimal(0),
                )
                balances.append(balance)

        for transform, exchange_ids in self.transforms.items():
            if self.exchange_id in exchange_ids:
                balances = transform(balances, self.asset_repository)

        return balances

    @staticmethod
    def should_ignore_asset(asset: str) -> bool:
        return asset.lower() == "nano"
