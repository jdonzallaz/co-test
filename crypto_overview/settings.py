from enum import Enum

from PyQt5 import QtCore
from PyQt5.QtCore import QSettings


class AssetDisplay(Enum):
    SYMBOL_AND_NAME = 0
    SYMBOL = 1
    NAME = 2


DEFAULT_SETTINGS = {
    "settings/nb_coins": 1000,
    "settings/dust_threshold": 10,
    "update_frequency/assets": 0,
    "update_frequency/balances": 300,
    "update_frequency/prices": 60,
    "tables/show_icons": int(True),
    "tables/asset_display": AssetDisplay.SYMBOL_AND_NAME.value,
    "state/hide_balance": int(False),
    "state/hide_dust": int(False),
    "state/currency": "usd",
    "wallets/wallets": [],
    "window/pos": QtCore.QPoint(10, 10),
    "window/size": QtCore.QSize(600, 700),
    "window/fullscreen": int(False),
    "app/version": "0.1.0",
}


def init_settings():
    settings = QSettings()

    for key, val in DEFAULT_SETTINGS.items():
        if not settings.contains(key):
            settings.setValue(key, val)

    settings.sync()
