from typing import Optional, Union

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt

from crypto_overview.models.asset import AssetRepository


class SettingsDialog(QtWidgets.QDialog):
    def __init__(self, parent, asset_repository: AssetRepository):
        super().__init__(parent, Qt.WindowCloseButtonHint)
        self.setWindowTitle("Settings")

        self.settings = QtCore.QSettings()
        self.asset_repository = asset_repository

        # Buttons
        self.button_box = QtWidgets.QDialogButtonBox(self)
        self.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Save)
        self.button_box.accepted.connect(self.save_and_accept)
        self.button_box.rejected.connect(self.reject)

        # Main layout
        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.setLayout(self.main_layout)

        self.controls = {}

        form_layout = self.add_group("General settings")
        self.controls["settings/nb_coins"] = self.add_spinbox(
            form_layout,
            "Number of coins",
            "settings/nb_coins",
            min=250,
            max=12500,
            step=250,
            help="The number of coins handled, selected by market share.\nUse the minimum necessary (steps of 250).",
        )
        self.controls["settings/dust_threshold"] = self.add_spinbox(
            form_layout,
            "Dust threshold",
            "settings/dust_threshold",
            min=1,
            max=1000000000,
            help='The limit below which a balance is considered "dust".\nIn base currency (USD by default).',
        )

        form_layout = self.add_group(
            "Update frequencies",
            "Data is updated automatically at fixed intervals.\n"
            "Too high a frequency can lead to errors due to the rate limits of the APIs.\n"
            '"0" disable the automatic update.',
        )
        self.controls["update_frequency/balances"] = self.add_spinbox(
            form_layout, "Balances update frequency [s]", "update_frequency/balances", min=0, max=604800
        )
        self.controls["update_frequency/prices"] = self.add_spinbox(
            form_layout, "Prices update frequency [s]", "update_frequency/prices", min=0, max=604800
        )
        self.controls["update_frequency/assets"] = self.add_spinbox(
            form_layout, "Assets update frequency [s]", "update_frequency/assets", min=0, max=604800
        )

        form_layout = self.add_group("Actions")
        button_refresh_assets = QtWidgets.QPushButton("Refresh")
        button_refresh_assets.pressed.connect(self.refresh_assets)
        form_layout.addRow("Refresh assets list", button_refresh_assets)

        form_layout = self.add_group("Tables settings")
        self.controls["tables/show_icons"] = self.add_checkbox(form_layout, "Icons", "Show icons", "tables/show_icons")
        self.controls["tables/asset_display"] = self.add_combobox(
            form_layout, "Asset text", ["Symbol and name", "Only symbol", "Only name"], "tables/asset_display"
        )

        self.main_layout.addWidget(self.button_box)

    def add_group(self, title: str, help: Optional[str] = None) -> QtWidgets.QFormLayout:
        group_box = QtWidgets.QGroupBox(title)
        self.main_layout.addWidget(group_box)
        form_layout = QtWidgets.QFormLayout()

        if help is None:
            group_box.setLayout(form_layout)
        else:
            vertical_layout = QtWidgets.QVBoxLayout()
            vertical_layout.addWidget(QtWidgets.QLabel(help))
            vertical_layout.addLayout(form_layout)
            group_box.setLayout(vertical_layout)
        return form_layout

    def add_spinbox(
        self,
        form_layout: QtWidgets.QFormLayout,
        label: str,
        settings_id: str,
        min: int = 0,
        max: int = 99,
        step: int = 1,
        help: Optional[str] = None,
    ) -> QtWidgets.QSpinBox:
        spinbox = QtWidgets.QSpinBox()
        spinbox.setRange(min, max)
        spinbox.setSingleStep(step)
        spinbox.setValue(self.settings.value(settings_id))

        control = self.add_help(spinbox, help)
        form_layout.addRow(label, control)

        return spinbox

    def add_checkbox(
        self,
        form_layout: QtWidgets.QFormLayout,
        label: str,
        box_label: str,
        settings_id: str,
        help: Optional[str] = None,
    ) -> QtWidgets.QCheckBox:
        checkbox = QtWidgets.QCheckBox(box_label)
        checkbox.setChecked(self.settings.value(settings_id))
        control = self.add_help(checkbox, help)
        form_layout.addRow(label, control)
        return checkbox

    def add_combobox(
        self,
        form_layout: QtWidgets.QFormLayout,
        label: str,
        items: list[str],
        settings_id: str,
        help: Optional[str] = None,
    ) -> QtWidgets.QComboBox:
        combobox = QtWidgets.QComboBox()
        combobox.addItems(items)
        combobox.setCurrentIndex(self.settings.value(settings_id))
        control = self.add_help(combobox, help)
        form_layout.addRow(label, control)
        return combobox

    def add_help(
        self, control: QtWidgets.QWidget, help: Optional[str] = None
    ) -> Union[QtWidgets.QLayout, QtWidgets.QWidget]:
        if help is not None:
            vertical_layout = QtWidgets.QVBoxLayout()
            vertical_layout.setSpacing(0)
            vertical_layout.addWidget(control)
            help_label = QtWidgets.QLabel(help)
            vertical_layout.addWidget(help_label)
            return vertical_layout

        return control

    def save_and_accept(self):
        for key, control in self.controls.items():
            value = ""
            if isinstance(control, QtWidgets.QSpinBox):
                value = control.value()
                # Verify bounds and step
                value = max(value, control.minimum())
                value = min(value, control.maximum())
                step = control.singleStep()
                value += (step - value) % step
            elif isinstance(control, QtWidgets.QCheckBox):
                value = int(control.isChecked())
            elif isinstance(control, QtWidgets.QComboBox):
                value = control.currentIndex()
            self.settings.setValue(key, value)

        self.settings.sync()
        self.accept()

    def refresh_assets(self):
        new_nb_coins = self.controls["settings/nb_coins"].value()
        self.settings.setValue("settings/nb_coins", new_nb_coins)
        self.settings.sync()
        self.asset_repository.update(ignore_cache=True)
