import csv
import logging
from pathlib import Path

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt

from crypto_overview import constants
from crypto_overview.models.price import PriceRepository
from crypto_overview.models.wallets import WalletRepository

logger = logging.getLogger()


class ExportDialog(QtWidgets.QDialog):
    def __init__(self, parent, wallet_repository: WalletRepository, price_repository: PriceRepository):
        super().__init__(parent, Qt.WindowCloseButtonHint)
        self.setWindowTitle("Export")

        self.wallet_repository = wallet_repository
        self.price_repository = price_repository

        # Buttons
        self.button_box = QtWidgets.QDialogButtonBox(self)
        self.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.button_box.button(QtWidgets.QDialogButtonBox.Ok).setText("Export")
        self.button_box.accepted.connect(self.export)
        self.button_box.rejected.connect(self.reject)

        # Main layout
        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.setLayout(self.main_layout)

        # Description
        description = QtWidgets.QLabel(
            "Export all balances in CSV format.\n"
            "All numbers are written with the selected maximum precision (highest is 45)."
        )

        # Form
        form = QtWidgets.QFormLayout()

        # Precision input
        self.precision_input = QtWidgets.QSpinBox()
        self.precision_input.setValue(45)
        self.precision_input.setRange(0, 45)
        self.precision_input.setSingleStep(1)
        form.addRow("Decimal precision", self.precision_input)

        # File input
        file_input = QtWidgets.QHBoxLayout()
        self.file_text = QtWidgets.QLineEdit()
        self.file_text.setMinimumWidth(200)
        default_path = Path.home() / f"{constants.NAME.lower()}-export.csv"
        self.file_text.setText(str(default_path))
        file_chooser = QtWidgets.QPushButton("Browse")
        file_chooser.clicked.connect(self.open_file_dialog)
        file_input.addWidget(self.file_text)
        file_input.addWidget(file_chooser)
        form.addRow("Export path", file_input)

        # Add widgets
        self.main_layout.addWidget(description)
        self.main_layout.addLayout(form)
        self.main_layout.addWidget(self.button_box)

    def open_file_dialog(self):
        # Get current path from line edit
        current_file_name = self.file_text.text()

        # Open file dialog to choose file path
        new_file_name = QtWidgets.QFileDialog.getSaveFileName(
            self, caption="Choose export path", directory=current_file_name, filter="CSV files (*.csv)"
        )

        # Save new file path to the line edit if not empty
        if new_file_name[0] != "":
            self.file_text.setText(new_file_name[0])

    def export(self):
        balances = self.wallet_repository.get_balances_per_asset_per_wallet()
        currency = self.price_repository.currency

        precision = self.precision_input.value()
        file_path = self.file_text.text()

        data = list()

        # Set headers
        data.append(
            [
                "Wallet name",
                "Wallet type",
                "Asset ID",
                "Asset symbol",
                "Asset name",
                "Free balance",
                "Locked balance",
                "Rewards balance",
                "Total balance",
                f"{currency.ticker} price",
                "24h change",
                f"{currency.ticker} value",
                "Share",
            ]
        )

        # Set balances
        for balance in balances:
            data.append(
                [
                    balance.wallet.name,
                    type(balance.wallet).__name__,
                    balance.asset.id,
                    balance.asset.symbol,
                    balance.asset.name,
                    "{:.{}f}".format(balance.free.normalize(), precision).rstrip("0").rstrip("."),
                    "{:.{}f}".format(balance.locked.normalize(), precision).rstrip("0").rstrip("."),
                    "{:.{}f}".format(balance.rewards.normalize(), precision).rstrip("0").rstrip("."),
                    "{:.{}f}".format(balance.total.normalize(), precision).rstrip("0").rstrip("."),
                    "{:.{}f}".format(balance.price.normalize(), precision).rstrip("0").rstrip("."),
                    "{:.{}f}".format(balance.change_24h.normalize(), precision).rstrip("0").rstrip("."),
                    "{:.{}f}".format(balance.value.normalize(), precision).rstrip("0").rstrip("."),
                    "{:.{}f}".format(balance.share.normalize(), precision).rstrip("0").rstrip("."),
                ]
            )

        try:
            # Create the path and the missing folders
            file_path = Path(file_path)
            file_path.parent.mkdir(parents=True, exist_ok=True)

            # Open file and save data
            with open(file_path, mode="w", newline="") as file:
                writer = csv.writer(file)
                writer.writerows(data)

            # Close dialog
            self.accept()

        except OSError as e:
            msg = f'Error while exporting to "{file_path}".'
            logger.error(msg, exc_info=e)
            message_box = QtWidgets.QMessageBox(
                QtWidgets.QMessageBox.Warning, "Export error", msg, QtWidgets.QMessageBox.Ok, self
            )
            message_box.setDetailedText(str(e))
            message_box.exec()
