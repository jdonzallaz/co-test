from dataclasses import fields
from typing import Optional

import ccxt
from PyQt5 import QtCore, QtWidgets

from crypto_overview.ui.components.wallet_forms.custom_wallet_form import CustomWalletForm
from crypto_overview.wallet.exchange_wallet import ExchangeConfig, ExchangeConfigType, ExchangeCredential


class ExchangeForm(CustomWalletForm):
    """
    Create and manage a small form for editing exchange config.
    """

    add_label_to_row: bool = False

    required_credentials_friendly_names = {
        "apiKey": "API key",
        "secret": "API secret",
        "uid": "UID",
        "login": "Login",
        "password": "Password",
        "twofa": "Two-FA",
        "privateKey": "Private key",
        "walletAddress": "Wallet address",
        "token": "Token",
    }

    def __init__(self, parent: Optional[QtCore.QObject] = None):
        super().__init__(parent)

        self.form = QtWidgets.QFormLayout()
        self.form.setContentsMargins(0, 0, 0, 0)
        self.form.setVerticalSpacing(0)

        self.combo = QtWidgets.QComboBox()
        for exchange_id in ccxt.exchanges:
            exchange_class = getattr(ccxt, exchange_id)
            self.combo.addItem(exchange_class().name, exchange_id)
        self.combo.currentIndexChanged.connect(self.exchange_changed)
        self.form.addRow("Exchange", self.combo)

        self.controls = {}
        for i, credential_field in enumerate(fields(ExchangeCredential)):
            if credential_field.name in self.required_credentials_friendly_names:
                label_str = self.required_credentials_friendly_names[credential_field.name]
            else:
                label_str = credential_field.name
            label = QtWidgets.QLabel(label_str)
            label.setContentsMargins(0, 5, 0, 0)
            line_edit = QtWidgets.QLineEdit()
            line_edit.setContentsMargins(0, 5, 0, 0)
            self.form.addRow(label, line_edit)
            self.controls[credential_field.name] = label, line_edit

        self.setLayout(self.form)

        self.combo.setCurrentIndex(0)
        self.exchange_changed()

    def set_data(self, exchange_config: ExchangeConfigType):
        self.combo.setCurrentIndex(self.combo.findData(exchange_config.id))

        exchange_class = getattr(ccxt, exchange_config.id)
        required_credentials = exchange_class().requiredCredentials

        for credential_name, (_, control) in self.controls.items():
            if required_credentials.get(credential_name, False):
                control.setText(getattr(exchange_config.credentials, credential_name))

    def get_data(self) -> ExchangeConfigType:
        exchange_id = self.combo.currentData()
        exchange_class = getattr(ccxt, exchange_id)
        required_credentials = exchange_class().requiredCredentials

        credentials = {}
        for credential_name, (_, control) in self.controls.items():
            if required_credentials.get(credential_name, False):
                credentials[credential_name] = control.text()

        return ExchangeConfig(id=exchange_id, credentials=ExchangeCredential(**credentials))

    def exchange_changed(self):
        exchange_id = self.combo.currentData()
        exchange_class = getattr(ccxt, exchange_id)
        required_credentials = exchange_class().requiredCredentials
        for credential_name, (label, control) in self.controls.items():
            if credential_name in required_credentials:
                if required_credentials[credential_name]:
                    label.show()
                    control.show()
                else:
                    label.hide()
                    control.hide()

    def clear(self):
        self.combo.setCurrentIndex(0)
        for _, (_, control) in self.controls.items():
            control.setText("")

    def get_default_data(self) -> ExchangeConfigType:
        return ExchangeConfig(ccxt.exchanges[0], ExchangeCredential())
