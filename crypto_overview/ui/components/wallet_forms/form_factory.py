from typing import Optional

from PyQt5 import QtCore

from crypto_overview.models.asset import AssetRepository
from crypto_overview.ui.components.wallet_forms.balances_form import BalancesForm
from crypto_overview.ui.components.wallet_forms.exchange_form import ExchangeForm


def create_wallet_form(class_name: str, asset_repository: AssetRepository, parent: Optional[QtCore.QObject] = None):
    if class_name == "BalancesForm":
        return BalancesForm(asset_repository, parent)
    elif class_name == "ExchangeForm":
        return ExchangeForm(parent)
