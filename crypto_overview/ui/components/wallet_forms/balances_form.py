from typing import Any, Optional

from PyQt5 import QtCore, QtWidgets

from crypto_overview.models.asset import AssetRepository
from crypto_overview.ui.components.wallet_forms.custom_wallet_form import CustomWalletForm
from crypto_overview.utils.widgets import remove_layout
from crypto_overview.wallet.custom_wallet import BalancesDictType


class BalancesForm(CustomWalletForm):
    """
    Create and manage a small form for adding/removing/editing assets balances.
    """

    def __init__(self, asset_repository: AssetRepository, parent: Optional[QtCore.QObject] = None):
        super().__init__(parent)

        self.asset_repository = asset_repository

        # V stack for form and buttons
        self.vstack_form = QtWidgets.QVBoxLayout()

        self.form_rows: list[QtWidgets.QLayout] = []
        self.form_controls: list[tuple[QtWidgets.QComboBox, QtWidgets.QLineEdit]] = []

        # Buttons
        row_buttons = QtWidgets.QHBoxLayout()
        add_button = QtWidgets.QPushButton("+")
        add_button.clicked.connect(self.add_row)
        row_buttons.addWidget(add_button)
        remove_button = QtWidgets.QPushButton("-")
        remove_button.clicked.connect(self.remove_row)
        row_buttons.addWidget(remove_button)

        vstack = QtWidgets.QVBoxLayout()
        vstack.setContentsMargins(0, 0, 0, 0)
        vstack.addLayout(self.vstack_form)
        vstack.addLayout(row_buttons)
        self.setLayout(vstack)

        self.add_row()

    def set_data(self, balances: BalancesDictType):
        self.set_nb_rows(len(balances))
        for i, (asset, quantity) in enumerate(balances.items()):
            combo, line_edit = self.form_controls[i]
            combo.setCurrentIndex(combo.findData(asset))
            line_edit.setText(str(quantity))

    def get_data(self) -> BalancesDictType:
        balances = {}
        for combo, line_edit in self.form_controls:
            balances[combo.currentData()] = line_edit.text()

        return balances

    def set_nb_rows(self, n: int):
        if n < 1:
            raise AttributeError("Cannot have a wallet with no balance.")
        while n != len(self.form_rows):
            if n < len(self.form_rows):
                self.remove_row()
            else:
                self.add_row()

    def add_row(self):
        # Row with combobox and line edit
        row = QtWidgets.QHBoxLayout()

        # Combobox of assets
        combo_assets = QtWidgets.QComboBox()
        for asset in self.asset_repository.get_assets(sort_by_name=True):
            combo_assets.addItem(f"{asset.name} [{asset.symbol.upper()}]", asset.id)
        row.addWidget(combo_assets)

        # Line edit with value
        line_edit = QtWidgets.QLineEdit()
        row.addWidget(line_edit)

        self.vstack_form.addLayout(row)
        self.form_rows.append(row)
        self.form_controls.append((combo_assets, line_edit))

    def remove_row(self):
        if len(self.form_rows) > 1:
            remove_layout(self.form_rows.pop())
            self.form_controls.pop()

    def clear(self):
        self.set_nb_rows(1)
        combo, line_edit = self.form_controls[0]
        combo.setCurrentIndex(0)
        line_edit.setText("")

    def get_default_data(self) -> BalancesDictType:
        return {"btc": 0.0}
