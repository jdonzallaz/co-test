from decimal import Decimal
from pathlib import Path
from typing import Any, Optional

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QObject, Qt

from crypto_overview.models.balance import WalletAssetPricedBalance
from crypto_overview.models.price import PriceRepository
from crypto_overview.models.wallets import WalletRepository
from crypto_overview.settings import AssetDisplay
from crypto_overview.utils.numbers_ import float_to_str


class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, wallet_repo: WalletRepository, price_repository: PriceRepository, parent=None):
        super().__init__(parent)
        self.wallet_repo = wallet_repo
        self.wallet_repo.add_observer(self.update_data)
        self.price_repository = price_repository
        self.price_repository.add_observer(self.on_price_update)
        self.settings = QtCore.QSettings()

        self.balances = []
        self.update_data()
        self.headers = self.get_headers()

    def on_price_update(self):
        self.headers = self.get_headers()
        self.headerDataChanged.emit(Qt.Horizontal, 0, self.columnCount() - 1)

    def get_headers(self) -> list[str]:
        ticker = self.price_repository.currency.ticker
        return ["Coin", "Wallet", "Amount", f"{ticker} price", "24h change", f"{ticker} value", f"Share"]

    def update_data(self):
        self.beginResetModel()
        self.balances: list[WalletAssetPricedBalance] = self.wallet_repo.get_balances_per_asset_per_wallet()
        self.endResetModel()

    def columnCount(self, parent=None):
        return len(self.headers)

    def rowCount(self, parent=None):
        return len(self.balances)

    def headerData(self, section: int, orientation: Qt.Orientation, role: int = None):
        if role == Qt.DisplayRole and orientation == Qt.Horizontal:
            return self.headers[section]

    def data(self, index: QtCore.QModelIndex, role: int = None):
        row = index.row()
        col = index.column()
        balance = self.balances[row]

        # First column - asset name, symbol and icon
        asset_display_setting = self.settings.value("tables/asset_display")
        if asset_display_setting == AssetDisplay.SYMBOL_AND_NAME.value:
            asset_display = f"{balance.asset.symbol.upper()}\n{balance.asset.name}"
        elif asset_display_setting == AssetDisplay.SYMBOL.value:
            asset_display = balance.asset.symbol.upper()
        else:  # asset_display_setting == AssetDisplay.NAME.value
            asset_display = balance.asset.name

        all_values = [asset_display, balance.wallet.get_name(), balance.total, balance.price, balance.change_24h,
                      balance.value, balance.share * 100]
        value = all_values[col]

        if col > 1:
            value = float(value)

        # Manage each role
        if role == Qt.DisplayRole:
            return value

        elif role == Qt.TextAlignmentRole:
            if isinstance(value, int) or isinstance(value, float) or isinstance(value, Decimal):
                return Qt.AlignVCenter | Qt.AlignRight

        elif role == Qt.DecorationRole:
            if col == 0 and self.settings.value("tables/show_icons"):
                icon_base_path = Path("resources/cryptocurrency-icons/32/color")
                balance = self.balances[row]
                asset_symbol = balance.asset.symbol
                icon_path = icon_base_path / f"{asset_symbol}.png"
                if not icon_path.exists():
                    icon_path = icon_base_path / "generic.png"
                return QtGui.QIcon(str(icon_path))

        elif role == Qt.ForegroundRole:
            if col == 4:
                if value < 0:
                    return QtGui.QColor('#c83d50')
                elif value > 0:
                    return QtGui.QColor('#00a59a')


class DustSortFilterProxyModel(QtCore.QSortFilterProxyModel):
    def __init__(self, col:int, hide_dust: bool = False, dust_threshold: Decimal = Decimal(0), parent: Optional[QObject] = None):
        super().__init__(parent)

        self.col = col
        self.hide_dust = hide_dust
        self.dust_threshold = dust_threshold
        self.settings = QtCore.QSettings()

    def filterAcceptsRow(self, row_num, parent):
        model = self.sourceModel()
        asset_total_value = model.index(row_num, self.col, parent).data()

        return not self.hide_dust or asset_total_value >= self.dust_threshold

    def set_hide_dust(self, hide_dust: bool):
        self.hide_dust = hide_dust
        self.invalidateFilter()

    def set_dust_threshold(self, dust_threshold: Decimal):
        self.dust_threshold = dust_threshold
        self.invalidateFilter()


class AnonymizerProxyModel(QtCore.QIdentityProxyModel):
    def __init__(self, hide_balance: bool = False, parent: Optional[QObject] = None):
        super().__init__(parent)

        self.hide_balance = hide_balance

    def data(self, index: QtCore.QModelIndex, role: int = None):
        col = index.column()

        if role == Qt.DisplayRole and self.hide_balance and col in [2, 5]:
            return "********"

        return super().data(index, role)

    def set_hide_balance(self, hide_balance: bool):
        self.layoutAboutToBeChanged.emit()
        self.hide_balance = hide_balance
        self.layoutChanged.emit()


class FormatPercentageProxyModel(QtCore.QIdentityProxyModel):
    def __init__(self, cols: set[int], parent: Optional[QObject] = None):
        super().__init__(parent)

        self.cols = cols

    def data(self, index: QtCore.QModelIndex, role: int = None):
        col = index.column()
        value = super().data(index, role)

        if role == Qt.DisplayRole and col in self.cols:
            return f"{value:.2f} %"

        return value


class RoundFloatDelegate(QtWidgets.QStyledItemDelegate):
    def __init__(self, parent=None):
        super().__init__(parent)

    def displayText(self, value: Any, locale: QtCore.QLocale) -> str:
        if isinstance(value, float):
            return float_to_str(value)

        return super().displayText(value, locale)
